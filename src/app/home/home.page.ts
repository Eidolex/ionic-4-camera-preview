import { CameraPreview } from '@ionic-native/camera-preview/ngx';
import { Component, ViewChild } from '@angular/core';
import { IonImg } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: [ 'home.page.scss' ],
})
export class HomePage {

  @ViewChild('image') image: IonImg;

  constructor(private cameraPreview: CameraPreview) { }

  async open() {
    try {
      const width = (window.screen.width / 2);
      const options = {
        x: width / 2,
        y: (window.screen.height / 2) - width,
        width,
        height: width,
        camera: this.cameraPreview.CAMERA_DIRECTION.FRONT,
        toBack: false,
        tapPhoto: true,
        tapFocus: false,
        previewDrag: false,
        storeToFile: false,
        disableExifHeaderStripping: false
      };
      const res = await this.cameraPreview.startCamera(options);
      console.log(res);
    } catch (error) {
      console.log(error);
    }
  }

  async takePicture() {
    try {
      const result = await this.cameraPreview.takeSnapshot({ quality: 85 });
      this.image.src = 'data:image/jpeg;base64,' + result;
    } catch (error) {
      console.log(error);
    }
  }
}
